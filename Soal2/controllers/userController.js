const { user } = require("../models");

class UserController {
  async getAll(req, res) {
    try {
      let data = await user.find();

      if (data.length === 0) {
        return res.status(400).json({
          message: "user not found",
        });
      }

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal server error!",
        error: e,
      });
    }
  }

  async getOne(req, res) {
    try {
      let data = await user.findOne({
        _id: req.params.id,
      });

      if (!data) {
        return res.status(400).json({
          message: "user not found!",
        });
      }

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal server error!",
        error: e,
      });
    }
  }

  async create(req, res) {
    try {
      let data = await user.create(req.body);

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async update(req, res) {
    try {
      let data = await user.findOneAndUpdate(
        {
          _id: req.params.id,
        },
        req.body,
        {
          new: true,
        }
      );

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async delete(req, res) {
    try {
      await user.delete({ _id: req.params.id });

      return res.status(200).json({
        message: "Success delete user",
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new UserController();
