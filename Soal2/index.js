require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

const express = require("express");
const app = express();
const userRoutes = require("./routes/userRoute");

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use("/user", userRoutes);

app.listen(3000, () => console.log("server running on http://localhost:3000"));
