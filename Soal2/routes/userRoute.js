const express = require("express");

const userValidator = require("../validators/userValidator");
const userController = require("../controllers/userController");

const router = express.Router();

router.get("/", userController.getAll);
router.get("/details/:id", userValidator.id_validator, userController.getOne);
router.post("/create", userValidator.create, userController.create);
router.put("/update/:id", userValidator.id_validator, userValidator.update_delete, userController.update);
router.delete("/delete/:id", userValidator.id_validator, userValidator.update_delete, userController.delete);

module.exports = router;