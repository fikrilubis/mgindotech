This is a [Express.js](https://expressjs.com/) project.

## Getting Started

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## API Endpoint

See the [Postman documentation](https://documenter.getpostman.com/view/12500906/TzefA3yK) for more details.

- ### Crate User

```bash
POST /user/create
```

- ### Get All User

```bash
GET /user
```

- ### Get One User

```bash
GET /user/details/:id
```

- ### Update User

```bash
PUT /user/update/:id
```

- ### Delete User

```bash
DELETE /user/delete/:id
```