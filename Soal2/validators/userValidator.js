const { user } = require("../models");
const mongoose = require("mongoose");

exports.create = async (req, res, next) => {
  try {
    let errors = [];

    if (req.body.nama.length == 0) {
      errors.push("Nama harus di isi");
    }

    if (req.body.hobi.length == 0) {
      errors.push("Hobi harus di isi");
    }

    if (req.body.alamat.length == 0) {
      errors.push("Alamat harus di isi");
    }

    if (req.body.nomor_telp.length == 0) {
      errors.push("Nomer telepon harus di isi");
    }

    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.id_validator = async (req, res, next) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json({
        message: "user ID is not valid",
      });
    }

    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.update_delete = async (req, res, next) => {
  try {
    let data = await user.findOne({
      _id: req.params.id,
    });

    if (!data) {
      return res.status(400).json({
        message: "user not found!",
      });
    }

    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
