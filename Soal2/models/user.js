const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const UserSchema = new mongoose.Schema({
  nama: {
    type: String,
    required: true,
  },
  hobi: {
    type: String,
    required: true,
  },
  alamat: {
    type: String,
    required: true,
  },
  nomor_telp: {
    type: Number,
    required: true,
  },
});

UserSchema.plugin(mongooseDelete, { overrideMethods: "all" });
module.exports = mongoose.model("user", UserSchema, "user");
