const produk = [
  {
    id_produk: 1,
    nama_produk: "Huawei P30 pro",
  },
  {
    id_produk: 2,
    nama_produk: "Huawei Nova 5T",
  },
];

const stok_produk = [
  {
    id_produk: 1,
    qty: 15,
  },
  {
    id_produk: 1,
    qty: 6,
  },
  {
    id_produk: 2,
    qty: 4,
  },
  {
    id_produk: 2,
    qty: 18,
  },
];

async function processData(produk, stok) {
  let data = [];
  await produk.forEach((element) => {
    let qty = 0;
    for(let i = 0; i < stok.length; i++){
        if(element.id_produk == stok[i].id_produk){
            qty += stok[i].qty;
        }
    }
    data.push({nama_produk: element.nama_produk, total_stock: qty});
  });

  let result = {
      hasil : data
  }
  console.log(result);
}

processData(produk, stok_produk);
